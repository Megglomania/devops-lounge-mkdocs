import itertools
import html

def define_env(env):

    @env.macro
    def code_in_file(file, line_start, line_end):
        with open('source/' + file, 'r') as fd:
            if line_start == -1 or line_end == -1:
                return fd.read()

            return_lines = ""
            for line in itertools.islice(fd, line_start, line_end):
                return_lines += line

            return return_lines

