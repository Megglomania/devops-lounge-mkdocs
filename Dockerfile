FROM nginx:latest

# Simply embed our website into an Nginx image - easy mode
COPY src/site/ /usr/share/nginx/html/